import React, {PropTypes} from 'react';
import {View} from 'react-native';
import {styles} from './styles';

const Spacer = () => (
  <View style={styles.spacer} />
);

export default Spacer;
