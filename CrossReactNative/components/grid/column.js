import React, {PropTypes} from 'react';
import {View} from 'react-native';
import {styles} from './styles';

const Column = ({children}) => (
  <View style={styles.column}>{children}</View>
);

Column.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default Column;
