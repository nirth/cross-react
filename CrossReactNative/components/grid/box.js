import React, {PropTypes} from 'react';
import {View} from 'react-native';
import {styles} from './styles';

const Box = ({children, style}) => (
  <View style={Object.assign({}, styles.box, style)}>{children}</View>
);

Box.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
  style: PropTypes.any,
};

Box.defaultProprs = {
  style: {},
};

export default Box;
