import React, {PropTypes} from 'react';
import {View} from 'react-native';
import {styles} from './styles';

const Row = ({children}) => (
  <View style={styles.row}>{children}</View>
);

Row.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element),
  ]),
};

export default Row;
