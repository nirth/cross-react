import {StyleSheet} from 'react-native';

const box = {
  flex: 1,
  justifyContent: 'flex-start',
  alignItems: 'stretch',
};

export const styles = StyleSheet.create({
  box,
  column: Object.assign({flexDirection: 'column'}, box),
  row: Object.assign({flexDirection: 'row'}, box),
  spacer: {flex: 1},
});
