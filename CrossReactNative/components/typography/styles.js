import {StyleSheet} from 'react-native';

const typography = {color: 'rgba(88, 67, 50, 1)'};

export const styles = StyleSheet.create({
  title: {fontSize: 26, color: 'rgba(88, 67, 50, 1)'},
  subtitle: {fontSize: 20, color: 'rgba(88, 67, 50, 1)'},
  label: {fontSize: 14, color: 'rgba(88, 67, 50, 1)'},
});
