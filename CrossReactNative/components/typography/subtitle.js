import React, {PropTypes} from 'react';
import {Text} from 'react-native';
import {styles} from './styles';

const Subtitle = ({children}) => (
  <Text style={styles.subtitle}>{children}</Text>
);

Subtitle.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Subtitle;
