import React, {PropTypes} from 'react';
import {Text} from 'react-native';
import {styles} from './styles';

const Label = ({children}) => (
  <Text style={styles.label}>{children}</Text>
);

Label.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Label;
