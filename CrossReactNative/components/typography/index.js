import Title from './title';
import Subtitle from './subtitle';
import Label from './label';

export {Title, Subtitle, Label};
