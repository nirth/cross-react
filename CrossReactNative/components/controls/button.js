import React, {PropTypes} from 'react';
import {TouchableOpacity} from 'react-native';
import {Label} from '../typography';
import {Box} from '../grid';

const resolveButtonLabel = (children, label) => {
  if (typeof children === 'string' && children.length > 0) {
    return children;
  } else if (typeof label === 'string' && label.length > 0) {
    return label;
  }

  return 'Label';
};

const Button = ({children, label, style, onClick}) => (
  <TouchableOpacity onPress={onClick}>
    <Box style={style}><Label>{resolveButtonLabel(children, label)}</Label></Box>
  </TouchableOpacity>
);

Button.propTypes = {
  children: PropTypes.string,
  label: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  label: null,
  children: null,
  style: {},
};

export default Button;
