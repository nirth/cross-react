import React, {PropTypes} from 'react';

const Repeater = ({items, parentRenderer, itemRenderer}) => {
  const Renderer = itemRenderer;
  const Parent = parentRenderer;
  const renderedItems = items.map((item, index) => <Renderer {...item} key={index} />);

  return <Parent>{renderedItems}</Parent>;
};

Repeater.propTypes = {
  parentRenderer: PropTypes.func.isRequired,
  itemRenderer: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
};

export default Repeater;
