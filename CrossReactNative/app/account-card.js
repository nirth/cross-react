import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Column, Row, Subtitle, Spacer, Label} from '../components';
import {accountCardSelectors} from './state';
import SavingsActions from './savings-actions';

const AccountCard = ({uuid, name, balance, savingsAccount, transactions}) => (
  <Column className="account card">
    <Subtitle>{name}</Subtitle>
    <Row>
      <Label>{`Balance: €${balance.toFixed(2)}`}</Label>
      <Spacer />
      {savingsAccount ? <Label>{`Status: ${transactions.length}`}</Label> : null}
    </Row>
    {savingsAccount ? <SavingsActions uuid={uuid} /> : null}
  </Column>
);

AccountCard.propTypes = {
  uuid: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  balance: PropTypes.number.isRequired,
  savingsAccount: PropTypes.bool.isRequired,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    fromAccount: PropTypes.string.isRequired,
    toAccount: PropTypes.string.isRequired,
    uuid: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
  })),
};

AccountCard.defaultProps = {
  currentAccount: false,
};

const mergeProps = ({transactions}, actions, props) => ({
  ...props,
  transactions: transactions.filter(({toAccount}) => toAccount === props.uuid),
});

export default connect(accountCardSelectors, null, mergeProps)(AccountCard);
