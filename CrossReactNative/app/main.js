import React from 'react';
import {Provider} from 'react-redux';
import {createAppStore} from './state';
import BankyBank from './banky-bank';

const Main = () => (
  <Provider store={createAppStore()}>
    <BankyBank />
  </Provider>
);

export default Main;
