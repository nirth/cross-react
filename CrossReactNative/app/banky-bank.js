import React from 'react';
import {Column, Title, Subtitle} from '../components';
import AccountsList from './accounts-list';

const BankyBank = () => (
  <Column className="app">
    <Title>Banky Bank</Title>
    <Subtitle>Simplifying Savings</Subtitle>
    <AccountsList />
  </Column>
);

export default BankyBank;
