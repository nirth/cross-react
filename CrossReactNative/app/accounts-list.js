import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Column, Repeater} from '../components';
import {accountsListSelectors} from './state';
import AccountCard from './account-card';

const AccountsList = ({accountsList}) => (
  <Repeater parentRenderer={Column} itemRenderer={AccountCard} items={accountsList} />
);

AccountsList.propTypes = {
  accountsList: PropTypes.arrayOf(PropTypes.shape({
    uuid: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    balance: PropTypes.number.isRequired,
    savingsAccount: PropTypes.bool.isRequired,
  })).isRequired,
};

export default connect(accountsListSelectors)(AccountsList);
