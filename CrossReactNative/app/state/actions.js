import {v4} from 'uuid';

export const CREATE_TRANSACTION = 'createTransaction';

export const createTransaction = (fromAccount, toAccount, amount) => (
  {type: CREATE_TRANSACTION, payload: {uuid: v4(), fromAccount, toAccount, amount}}
);
