import {combineReducers} from 'redux';
import {CREATE_TRANSACTION} from './actions';
import {copyObject} from '../../components';

const accounts = (state = {}, {type, payload}) => {
  // Action mutations associated with accounts.
  switch (type) {
    case CREATE_TRANSACTION: {
      const {fromAccount, toAccount, amount} = payload;
      // Create updated version of from account with new balance.
      const f = state[fromAccount];
      const nextFromAccount = copyObject(f, {balance: f.balance - amount});
      // Create updated version of to account with new balance.
      const t = state[toAccount];
      const nextToAccount = copyObject(t, {balance: t.balance + amount});

      return copyObject(state, {
        [fromAccount]: nextFromAccount,
        [toAccount]: nextToAccount,
      });
    }
    default:
      return state;
  }
};

const transactions = (state = {}, {type, payload}) => {
  // Actions mutations associated with transactions.
  switch (type) {
    case CREATE_TRANSACTION: {
      return copyObject(state, {[payload.uuid]: payload});
    }
    default:
      return state;
  }
};

export const createAppReducers = () => combineReducers({accounts, transactions});
