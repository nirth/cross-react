import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {Row, Button} from '../components';
import {createTransaction, savingsActionsSelectors} from './state';

const SavingsActions = ({saveMoney}) => (
  <Row>
    <Button label="Save 25" onClick={() => saveMoney(25)} />
    <Button label="Save 50" onClick={() => saveMoney(50)} />
    <Button label="Save 100" onClick={() => saveMoney(100)} />
    <Button label="Save 200" onClick={() => saveMoney(200)} />
  </Row>
);

SavingsActions.propTypes = {
  uuid: PropTypes.string.isRequired,
  saveMoney: PropTypes.func.isRequired,
};

const mergeProps = ({currentAccount}, actions, {uuid}) => {
  const saveMoney = actions.createTransaction.bind(null, currentAccount.uuid, uuid);

  return {saveMoney, uuid};
};

export default connect(savingsActionsSelectors, {createTransaction}, mergeProps)(SavingsActions);
