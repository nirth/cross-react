import {createStore} from 'redux';
import {createAppReducers} from './reducers';
import {v4} from 'uuid';

const createRandomAccount = (name, max, savingsAccount = true) => ({
  name,
  uuid: v4(),
  balance: Math.round(Math.random() * (max * 10)) / 10,
  savingsAccount,
});

export const createAppStore = () => {
  const currentAccount = createRandomAccount('Current', 2000, false);
  const savingsAccount = createRandomAccount('Savings', 15000);
  const savingForPorsche = createRandomAccount('Saving for Porsche', 7000);
  const savingForLaptop = createRandomAccount('Saving for Laptop', 1000);
  const savingForHomeOffice = createRandomAccount('Saving for Home Office', 5000);

  return createStore(
    createAppReducers(),
    {
      accounts: {
        [currentAccount.uuid]: currentAccount,
        [savingsAccount.uuid]: savingsAccount,
        [savingForPorsche.uuid]: savingForPorsche,
        [savingForLaptop.uuid]: savingForLaptop,
        [savingForHomeOffice.uuid]: savingForHomeOffice,
      },
      transactions: {},
    },
    window.devToolsExtension && window.devToolsExtension(),
  );
};
