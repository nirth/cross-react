import {createSelector} from 'reselect';

const toList = (o) => Object.keys(o).map((uuid) => o[uuid]);

const selectAccounts = (state) => state.accounts;

const selectTransactions = (state) => state.transactions;

const selectAccountsAsList = createSelector(
  [selectAccounts], (accounts) => toList(accounts)
);

const selectCurrentAccount = createSelector(
  [selectAccountsAsList],
  (accountsList) => accountsList.filter(({savingsAccount}) => !savingsAccount).shift(),
);

const selectTransactionsList = createSelector(
  [selectTransactions], (transactions) => toList(transactions)
);

const selectTransactionsFromCurrentAccount = createSelector(
  [selectTransactionsList, selectCurrentAccount],
  (transactions, {uuid}) => transactions.filter(({fromAccount}) => fromAccount === uuid)
);

export const accountsListSelectors = (state) => ({
  accountsList: selectAccountsAsList(state),
});

export const accountCardSelectors = (state) => ({
  transactions: selectTransactionsFromCurrentAccount(state),
});

export const savingsActionsSelectors = (state) => ({
  currentAccount: selectCurrentAccount(state),
});
