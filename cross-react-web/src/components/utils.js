
export const copyObject = (object, additionalProperties) => (
  Object.assign({}, object, additionalProperties)
);

export const copyArray = (array, ...items) => (
  array.concat(items)
);
