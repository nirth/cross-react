import React, {PropTypes} from 'react';

const Spacer = ({className}) => (
  <div className={`grid spacer ${className}`} />
);

Spacer.propTypes = {
  className: PropTypes.string,
};

Spacer.defaultProps = {
  chassName: '',
};

export default Spacer;
